<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/identidad', [App\Http\Controllers\CommonController::class, 'identidad']);


//MODULE PERFIL USUARIO
Route::group(['prefix'=>'perfil','as'=>'perfil.'], function(){
    Route::get('/', [App\Http\Controllers\PerfilController::class, 'index'])->name('index');
    Route::post('/save', [App\Http\Controllers\PerfilController::class, 'store'])->name('save');
});


//MODULE VISITAS
Route::group(['prefix'=>'visitas','as'=>'visitas.'], function(){
    Route::get('/', [App\Http\Controllers\VisitaController::class, 'index'])->name('index');

    Route::get('/publico', [App\Http\Controllers\VisitaController::class, 'publico'])->name('publico');

    Route::get('/selects', [App\Http\Controllers\VisitaController::class, 'selects'])->name('selects');
    Route::get('/selectEr', [App\Http\Controllers\VisitaController::class, 'selectEr'])->name('selectEr');


    Route::get('/selectsTipoVisita', [App\Http\Controllers\VisitaController::class, 'seselectsTipoVisitalects'])->name('selectsTipoVisita');

    Route::get('/edit/{id}', [App\Http\Controllers\VisitaController::class, 'edit'])->name('edit');


    //POST
    Route::post('/updateEr', [App\Http\Controllers\VisitaController::class, 'updateEr'])->name('updateEr');

    Route::post('/marcar', [App\Http\Controllers\VisitaController::class, 'marcar'])->name('marcar');
    Route::post('/create', [App\Http\Controllers\VisitaController::class, 'store'])->name('create');
    Route::post('/update', [App\Http\Controllers\VisitaController::class, 'update'])->name('update');
    Route::post('/delete', [App\Http\Controllers\VisitaController::class, 'destroy'])->name('delete');

});

//MODULE HERRAMIENTA COMPLEX BY VISITAS
Route::group(['prefix'=>'herramientas','as'=>'herramientas.'], function(){

    //POST
    Route::post('/delete', [App\Http\Controllers\HerramientaController::class, 'destroy'])->name('delete');

});

//MODULE ENTIDAD
Route::group(['prefix'=>'entidad','as'=>'entidad.'], function(){
    Route::get('/', [App\Http\Controllers\EntidadController::class, 'index'])->name('index');
    //POST
    Route::post('/save', [App\Http\Controllers\EntidadController::class, 'store'])->name('save');

    Route::post('/update', [App\Http\Controllers\EntidadController::class, 'update'])->name('update');

    Route::post('/delete', [App\Http\Controllers\EntidadController::class, 'destroy'])->name('delete');

});

//MODULE ROLES
Route::group(['prefix'=>'rol','as'=>'rol.'], function(){
    Route::get('/', [App\Http\Controllers\RolController::class, 'index'])->name('index');
    //POST
    Route::post('/save', [App\Http\Controllers\RolController::class, 'store'])->name('save');

    Route::post('/update', [App\Http\Controllers\RolController::class, 'update'])->name('update');
});

//MODULE OFICINAS
Route::group(['prefix'=>'oficinas','as'=>'oficinas.'], function(){
    Route::get('/', [App\Http\Controllers\OficinaController::class, 'index'])->name('index');
    //POST
    Route::post('/save', [App\Http\Controllers\OficinaController::class, 'store'])->name('save');

    Route::post('/update', [App\Http\Controllers\OficinaController::class, 'update'])->name('update');

    Route::post('/delete', [App\Http\Controllers\OficinaController::class, 'destroy'])->name('delete');

});

//MODULE CARGOS
Route::group(['prefix'=>'cargos','as'=>'cargos.'], function(){
    Route::get('/', [App\Http\Controllers\CargoController::class, 'index'])->name('index');
    //POST
    Route::post('/save', [App\Http\Controllers\CargoController::class, 'store'])->name('save');

    Route::post('/update', [App\Http\Controllers\CargoController::class, 'update'])->name('update');

    Route::post('/delete', [App\Http\Controllers\CargoController::class, 'destroy'])->name('delete');

});

//MODULE TIPOVISITAS
Route::group(['prefix'=>'tipovisitas','as'=>'tipovisitas.'], function(){
    Route::get('/', [App\Http\Controllers\TipoVisitaController::class, 'index'])->name('index');
    //POST
    Route::post('/save', [App\Http\Controllers\TipoVisitaController::class, 'store'])->name('save');

    Route::post('/update', [App\Http\Controllers\TipoVisitaController::class, 'update'])->name('update');

    Route::post('/delete', [App\Http\Controllers\TipoVisitaController::class, 'destroy'])->name('delete');

});

//MODULE MOTIVOS
Route::group(['prefix'=>'motivos','as'=>'motivos.'], function(){
    Route::get('/', [App\Http\Controllers\MotivoController::class, 'index'])->name('index');
    //POST
    Route::post('/save', [App\Http\Controllers\MotivoController::class, 'store'])->name('save');

    Route::post('/update', [App\Http\Controllers\MotivoController::class, 'update'])->name('update');

    Route::post('/delete', [App\Http\Controllers\MotivoController::class, 'destroy'])->name('delete');

});

//MODULE SEDES
Route::group(['prefix'=>'sedes','as'=>'sedes.'], function(){
    Route::get('/', [App\Http\Controllers\SedeController::class, 'index'])->name('index');
    //POST
    Route::post('/save', [App\Http\Controllers\SedeController::class, 'store'])->name('save');

    Route::post('/update', [App\Http\Controllers\SedeController::class, 'update'])->name('update');

    Route::post('/delete', [App\Http\Controllers\SedeController::class, 'destroy'])->name('delete');

});

//MODULE EMPLEADOS
Route::group(['prefix'=>'empleados','as'=>'empleados.'], function(){

    Route::get('/', [App\Http\Controllers\EmpleadosController::class, 'index'])->name('index');

    Route::get('/selects', [App\Http\Controllers\EmpleadosController::class, 'selects'])->name('selects');
    Route::get('/edit/{id}', [App\Http\Controllers\EmpleadosController::class, 'edit'])->name('edit');

    //POST
    Route::post('/save', [App\Http\Controllers\EmpleadosController::class, 'store'])->name('save');
    Route::post('/create', [App\Http\Controllers\EmpleadosController::class, 'store'])->name('create');
    Route::post('/update', [App\Http\Controllers\EmpleadosController::class, 'update'])->name('update');

    Route::post('/delete', [App\Http\Controllers\EmpleadosController::class, 'destroy'])->name('delete');

});

//MODULE USERS
Route::group(['prefix'=>'users','as'=>'users.'], function(){

    Route::get('/', [App\Http\Controllers\UserController::class, 'index'])->name('index');
    Route::get('/rolesall', [App\Http\Controllers\UserController::class, 'rolesAll'])->name('rolesall');
    Route::get('/edit/{id}', [App\Http\Controllers\UserController::class, 'edituser'])->name('edituser');

    //POST
    Route::post('/save', [App\Http\Controllers\UserController::class, 'store'])->name('save');
    Route::post('/update', [App\Http\Controllers\UserController::class, 'update'])->name('update');
    Route::post('/delete', [App\Http\Controllers\UserController::class, 'destroy'])->name('delete');

});
