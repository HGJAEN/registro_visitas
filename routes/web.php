<?php

use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/clear-cache', function () {
    echo Artisan::call('config:clear');
    echo Artisan::call('config:cache');
    echo Artisan::call('route:clear');
    echo Artisan::call('route:cache');
    echo Artisan::call('view:clear');
    echo Artisan::call('view:cache');
    echo Artisan::call('event:clear');
    echo Artisan::call('event:cache');

 });


Auth::routes(["register" => false]);

Route::get('/pruebas', [App\Http\Controllers\UserController::class, 'pruebas'])->name('pruebasweb');


Route::get('/{any?}',[App\Http\Controllers\PageController::class, 'index'])->where('any', '.*');

Route::post('/reniec', [App\Http\Controllers\ApiController::class, 'reniec'])->name('reniec');
Route::post('/sunat', [App\Http\Controllers\ApiController::class, 'sunat'])->name('sunat');