<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Cargo;
use Illuminate\Support\Facades\Validator;
use Yajra\Datatables\Datatables;

class CargoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $table = Datatables::of(Cargo::all());
        $table->editColumn('created_at', function ($request) {
            return $request->created_at->format('d M Y - h:i a');
        });
        $table->addColumn('action', function ($row) {
            return '';
        })->rawColumns(['action']);
        return $table->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate(
            [
                'cargo' => 'required|unique:cargos',
            ],
            [
                'cargo.required' => 'El cargo es obligatorio.'
            ]
        );

        $all = $request->all();
        $all['cargo'] = strtoupper($all['cargo']);
        $table = new Cargo();
        $table->fill($all)->save();
        return response()->json(['success' => 'Registro agregado con exito']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        request()->validate(
            [
                'cargo' => 'required',
            ],
            [
                'cargo.required' => 'El cargo es obligatorio.'
            ]
        );
        $all = $request->all();
        $all['cargo'] = strtoupper($all['cargo']);
        $table = Cargo::find($all['id']);
        $table->fill($all)->save();
        return response()->json(['success' => 'Registro actualizado con exito']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $all = $request->all();
        $table = Cargo::find($all['id_data']);
        if (!empty($table)) {
            $table->delete();
        }
    }
}
