<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Tipovisita;
use Yajra\Datatables\Datatables;

class TipoVisitaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $table = Datatables::of(Tipovisita::all());
        $table->editColumn('created_at', function ($request) {
            return $request->created_at->format('d M Y - h:i a');
        });
        $table->addColumn('action', function ($row) {
            return '';
        })->rawColumns(['action']);
        return $table->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate(
            [
                'nombre' => 'required|unique:tipovisitas',
            ],
            [
                'nombre.required' => 'El nombre es obligatorio.'
            ]
        );

        $all = $request->all();
        $all['nombre'] = strtoupper($all['nombre']);
        $table = new Tipovisita();
        $table->fill($all)->save();
        return response()->json(['success' => 'Registro agregado con exito']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        request()->validate(
            [
                'nombre' => 'required|unique:tipovisitas',
            ],
            [
                'nombre.required' => 'El nombre es obligatorio.',
                'nombre.unique:tipovisitas' => 'El tipo de visita ya se encuentra registrado, ¡Verificar!'
            ]
        );
        $all = $request->all();
        $all['nombre'] = strtoupper($all['nombre']);
        $table = Tipovisita::find($all['id']);
        $table->fill($all)->save();
        return response()->json(['success' => 'Registro actualizado con exito']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $all = $request->all();
        $table = Tipovisita::find($all['id_data']);
        if (!empty($table)) {
            $table->delete();
        }
    }
}
