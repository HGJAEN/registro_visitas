<?php

namespace App\Http\Controllers;

use App\Models\Visita;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Validator;

class ApiController extends Controller
{
    public function reniec()
    {
        request()->validate(
            [
                'dni' => 'required|size:8'
            ],
            [
                'dni.required' => 'El documento de identificación es obligatorio',
            ]
        );
        $documento = request('dni');

        $consultalocal = Visita::where('dni', $documento)
            ->where('fecha', '>', '2023-06-01')
            ->where('apiStatus', '=', '1')
            ->value('nombre');

        if ($consultalocal != null) {
            return response()->json([
                'nombres' => $consultalocal,
                'dni' => $documento,
                'consulta' => 'local'
            ]);
        } else {
            $token = config('services.apisunat.token');
            $urldni = config('services.apisunat.urldni');

            $response = Http::withHeaders([
                'Referer' => 'https://apis.net.pe/consulta-dni-api',
                'Authorization' => 'Bearer ' . $token
            ])->get($urldni . $documento);
            $api = json_decode($response);

            if (isset($response['nombres']) &&  ($response['nombres']) != "") {
                return response()->json([
                    'nombres' => $api->nombres . ' ' . $api->apellidoPaterno . ' ' . $api->apellidoMaterno,
                    'dni' => $api->numeroDocumento,
                    'consulta' => 'reniec'
                ]);
            } else if(($response['nombres']) == "") {
                return response()->json([
                    'nombres' => '',
                    'status' => 'false'
                ]);
            } else if(($response['message']) == "not found") { 
                return response()->json([
                    'nombres' => '',
                    'status' => 'not found'
                ]);
            }else {
                return response()->json([
                    'message' => 'No existe',
                    'errors' => [
                        'dni' => [
                            'DNI no encontrado en la RENIEC ¡Verificar!'
                        ]
                    ],
                ], 422);
            }
        }
    }

    public function sunat()
    {
        request()->validate(
            [
                'identificacion' => 'required|size:11'
            ],
            [
                'identificacion.required' => 'El campo RUC es obligatorio'
            ]
        );
        $documento = request('identificacion');

        $token = config('services.apisunat.token');
        $urlruc = config('services.apisunat.urlruc');

        $response = Http::withHeaders([
            'Referer' => 'http://apis.net.pe/api-ruc',
            'Authorization' => 'Bearer ' . $token
        ])->get($urlruc . $documento);

        $api = json_decode($response);

        if (isset($response['error'])) {
            return response()->json([
                'message' => 'No existe',
                'errors' => [
                    'identificacion' => [
                        'RUC No encontrado en la SUNAT ¡Verificar!'
                    ]
                ],
            ], 422);
        } else {
            return response()->json([
                'identificacion' => $api->numeroDocumento,
                'nsocial' => $api->nombre,
                'nombres' => ' - - ',
                'apellidos' => ' - -',
                'direccion' => $api->direccion . ' - ' . $api->departamento . ' - ' . $api->provincia . ' - ' . $api->distrito,
            ]);
        }
    }
}
