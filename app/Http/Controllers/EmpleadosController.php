<?php

namespace App\Http\Controllers;

use App\Models\Cargo;
use App\Models\Oficina;

use App\Models\Empleado;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Validator;

class EmpleadosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $empleado = new Empleado();
        $table = Datatables::of($empleado->mapempleado($request));
        $table->editColumn('created_at', function ($request) {
            return $request->created_at->format('d M Y - h:i a');
        });
        $table->addColumn('action', function ($row) {
            return '';
        })->addColumn('cargo', function ($row) {
            if (!empty($row['cargo'])) {
                return $row['cargo'];
            }
            return '';
        })->rawColumns(['action']);

        return $table->make(true);
    }

    public function selects()
    {
        return [
            'oficinas' => Oficina::all(),
            'cargos'=> Cargo::all(),
        ];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $all = $request->all();

        $validator = Validator::make(
            $request->all(),
            [
                'dni' => 'required|unique:empleados',
                'empleado' => 'required',
                'itemsjson' => 'required'
            ],
            [
                'itemsjson.required' => 'Elegir Officina'
            ]
        );
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->all()], 422);
        } else {
            $all['itemsjson'] = json_encode($all['itemsjson']);
            $table = new Empleado();

            $table->fill($all)->save();
            
            // $request->session()->flash('message_success', 'Registro con el nombre ' . $all['nombre'] . ' agregado con exito!');
            return response()->json(['success' => 'Registro agregado con exito']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $empleado = new Empleado();
        return $empleado->mapEmpleadoById($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $all = $request->all();
        $validator = Validator::make($request->all(), 
        [
            'dni' => 'required',
            'empleado' => 'required',
            'itemsjson' => 'required'
        ],
        [
            'itemsjson.required' => 'Elegir Officina'
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->all()]);
        } else {
            $empleado = Empleado::find($all['id']);

            $empleado->fill($all)->save();

            $request->session()->flash('message_success', 'Registro actualizado con exito!');

            return response()->json(['success' => 'Registro actualizado con exito']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $all = $request->all();
        $entidad = Empleado::find($all['id_data']);
        if (!empty($entidad)) {
            $entidad->delete();
        }
    }
}
