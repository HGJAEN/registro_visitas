<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Validator;

class CommonController extends Controller
{
    public function identidad(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'dni' => 'required|digits:8',
        ]);
        if ($validator->fails()) return response()->json($validator->errors(), 422);

        $res = Http::withoutVerifying()->get(env('API_SUNAT_DNI'), [
            'numero' => $request->dni,
        ]);

        if($res->ok()) {
            $identidad = $res->json();
            $data = [
                'ApellidoPaterno' => $identidad['apellidoPaterno'],
                'ApellidoMaterno' => $identidad['apellidoMaterno'],
                'Nombres' => $identidad['nombres'],
            ];
            return response()->json([
                'status' => true,
                'data' => $data,
            ]);
        }else {
            return response()->json([
                'status' => false,
                'message' => 'No se encontraron resultados'
            ]);
        }
    }
}
