<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Empleado extends Model
{
    use HasFactory;
    protected $fillable = [
        'dni',
        'empleado',
        'itemsjson'
    ];

    public function mapempleado($request){
        $collect =  Empleado::all();
        
        $collect->map(function($item, $key) {
            if (!empty($item->itemsjson)) {
                $json = json_decode($item->itemsjson);
                foreach ($json as $key => $val) {
                    $item->$key = $val->name;
                }
            }
        });
        return $collect;
    }

    public function mapEmpleadoById($id){
        $item =  Empleado::find($id); 
        if (!empty($item->itemsjson)) {
            $json = json_decode($item->itemsjson);
            $item->itemsjson = $json;
        }
        return $item;
    }
}
