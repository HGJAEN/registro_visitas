<!DOCTYPE html>
<html lang="<?php echo e(str_replace('_', '-', app()->getLocale())); ?>">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Registro de visitas</title>

    <script>
        let base_path_vue_router = '';
        let base_path_asset = "<?php echo e(asset('')); ?>";
    </script>
    <?php if(!empty(Config::get('vueconfig.path'))): ?>
        <script>
            base_path_vue_router = "<?php echo e(Config::get('vueconfig.path')); ?>";
        </script>
    <?php endif; ?>

    <!--BYR-->
    <?php echo app('Tightenco\Ziggy\BladeRouteGenerator')->generate(); ?>
    
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,300i,400,500,700&display=swap" rel="stylesheet">
    <link href="<?php echo e(asset('css/app.css')); ?>" rel="stylesheet">

</head>

<body>
    <nav class="navbar navbar-primary">
        <div class="container">
            <h5 class="navbar-brand">Hospital General de Jaén</h5>
            <div class="d-flex">
                <?php if(Route::has('login')): ?>
                    <?php if(auth()->guard()->check()): ?>
                        <span class="navbar-text">
                            <a class="btn btn-secondary text-white" href="<?php echo e(url('/home')); ?>">Inicio</a>
                        </span>
                    <?php else: ?>
                        <span class="navbar-text">
                            <a class="btn btn-primary  text-white" href="<?php echo e(route('login')); ?>">Iniciar Sesión</a>
                        </span>
                    <?php endif; ?>
                <?php endif; ?>
            </div>
        </div>
    </nav>

    

    <?php $__env->startSection('content'); ?>
        <!-- Main content -->
        <div class="container-fluid px-md-5">
            <h3 class="h3 fw-bold text-info">Reporte de visitas</h3>
            <?php if(session('message_success')): ?>
                <div class="container mt-1" id="session_message">
                    <div class="alert alert-success"><i class="bi bi-check-circle"></i> <?php echo e(session('message_success')); ?>

                    </div>
                </div>
            <?php endif; ?>

            <example-component></example-component>
        </div>
        <!-- /.content -->
    <?php $__env->stopSection(); ?>

</body>

</html>

<?php echo $__env->make('publico', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\laragon\www\registro_visitas\resources\views/welcome.blade.php ENDPATH**/ ?>