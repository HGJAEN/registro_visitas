<div class="wrapper">
  <!-- Preloader -->
  <div class="preloader flex-column justify-content-center align-items-center">
      <img class="animation__shake" src="<?php echo e(asset('img/landing/logo2.jpg')); ?>"  height="200" width="400">
  </div>
  <div id="app">
      <?php echo $__env->make('template_parts.nav', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    
      <?php echo $__env->make('template_parts.sidebar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
      <div class="content-wrapper">
          <div id="app">
            <!-- /.content-header -->
            <modal-profile :asset="'<?php echo e(asset('')); ?>'"></modal-profile>

            <?php echo $__env->yieldContent('content'); ?>
          </div>
      </div>
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <strong>Copyright &copy; 2021-2022</strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
      <b>Version</b> 1.0
    </div>
  </footer>
</div><?php /**PATH C:\laragon\www\registro_visitas\resources\views/template_parts/vue_config.blade.php ENDPATH**/ ?>