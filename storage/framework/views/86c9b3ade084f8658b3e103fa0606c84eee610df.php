

<?php $__env->startSection('content'); ?>
<!-- Main content -->
<?php if(session('message_success')): ?>
	<div class="container-fluid mt-1" id="session_message">
		<div class="alert alert-success"><i class="bi bi-check-circle"></i> <?php echo e(session('message_success')); ?></div>
	</div>
<?php endif; ?>

<router-view></router-view>
<!-- /.content -->
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\laragon\www\registro_visitas\resources\views\home.blade.php ENDPATH**/ ?>