<!-- Main Sidebar Container -->
<aside class="main-sidebar bg-light elevation-2">
    <!-- Brand Logo -->
    <a href="<?php echo e(url('/')); ?>" class="brand-link shadow-sm">
        <img src="<?php echo e(asset('img/visitas2.png')); ?>" alt="ico" class="brand-image img-circle" style="opacity: .8">
        <span class="brand-text fw-semibold text-primary"> <?php echo e('Reg. de visitas'); ?></span>
    </a>
    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        
        <!-- Sidebar Menu -->
        <nav class="mt-3">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
                data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class with font-awesome or any other icon font library -->

                <li class="nav-item">
                    <router-link to="/home" class="nav-link">
                        <i class="nav-icon bi bi-grid-3x3-gap-fill"></i>
                        <p>
                            Inicio
                        </p>
                    </router-link>
                </li>
                <li class="nav-item">
                    <router-link to="/visitas" class="nav-link ">
                        <i class="nav-icon bi bi-eye"></i>
                        <p>
                            Visitas
                        </p>
                    </router-link>
                </li>
                <li class="nav-item">
                    <router-link to="/visitaprogramada" class="nav-link ">
                        <i class="nav-icon bi bi-eye-fill"></i>
                        <p>
                            Visitas Programadas
                        </p>
                    </router-link>
                </li>
                <?php if(auth()->check() && auth()->user()->hasRole('admin')): ?>
                    <li class="nav-item">
                        <router-link to="/tipovisitas" class="nav-link">
                            <i class="bi bi-card-checklist"></i>
                            <p>
                                Tipos de Visita
                            </p>
                        </router-link>
                    </li>
                    <li class="nav-item">
                        <router-link to="/empleados" class="nav-link">
                            <i class="bi bi-person-lines-fill"></i>
                            <p>
                                Empleados
                            </p>
                        </router-link>
                    </li>
                    <li class="nav-item">
                        <router-link to="/oficinas" class="nav-link">
                            <i class="nav-icon bi bi-person-workspace"></i>
                            <p>
                                Oficinas
                            </p>
                        </router-link>
                    </li>
                    <li class="nav-item">
                        <router-link to="/cargos" class="nav-link">
                            <i class="bi bi-award"></i>
                            <p>
                                Cargos
                            </p>
                        </router-link>
                    </li>
                    <li class="nav-item">
                        <router-link to="/entidad" class="nav-link">
                            <i class="nav-icon bi bi-building"></i>
                            <p>
                                Entidad
                            </p>
                        </router-link>
                    </li>
                    <li class="nav-item">
                        <router-link to="/motivos" class="nav-link">
                            <i class="nav-icon bi bi-bookmarks-fill"></i>
                            <p>
                                Motivos
                            </p>
                        </router-link>
                    </li>
                    <li class="nav-item">
                        <router-link to="/usuarios" class="nav-link">
                            <i class="nav-icon bi bi-people-fill"></i>
                            <p>
                                Usuarios
                            </p>
                        </router-link>
                    </li>
                    <li class="nav-item">
                        <router-link to="/sedes" class="nav-link">
                            <i class="nav-icon bi bi-hospital"></i>
                            <p>
                                Sedes
                            </p>
                        </router-link>
                    </li>

                    <?php elseif(auth()->check() && auth()->user()->hasRole('Administrator')): ?>
                    <li class="nav-item">
                        <router-link to="/tipovisitas" class="nav-link">
                            <i class="bi bi-card-checklist"></i>
                            <p>
                                Tipos de Visita
                            </p>
                        </router-link>
                    </li>
                    <li class="nav-item">
                        <router-link to="/empleados" class="nav-link">
                            <i class="bi bi-person-lines-fill"></i>
                            <p>
                                Empleados
                            </p>
                        </router-link>
                    </li>
                    <li class="nav-item">
                        <router-link to="/oficinas" class="nav-link">
                            <i class="nav-icon bi bi-person-workspace"></i>
                            <p>
                                Oficinas
                            </p>
                        </router-link>
                    </li>
                    <li class="nav-item">
                        <router-link to="/cargos" class="nav-link">
                            <i class="bi bi-award"></i>
                            <p>
                                Cargos
                            </p>
                        </router-link>
                    </li>
                    <li class="nav-item">
                        <router-link to="/entidad" class="nav-link">
                            <i class="nav-icon bi bi-building"></i>
                            <p>
                                Entidad
                            </p>
                        </router-link>
                    </li>
                    <li class="nav-item">
                        <router-link to="/motivos" class="nav-link">
                            <i class="nav-icon bi bi-bookmarks-fill"></i>
                            <p>
                                Motivos
                            </p>
                        </router-link>
                    </li>
                    <li class="nav-item">
                        <router-link to="/usuarios" class="nav-link">
                            <i class="nav-icon bi bi-people-fill"></i>
                            <p>
                                Usuarios
                            </p>
                        </router-link>
                    </li>
                <?php endif; ?>

            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
<?php /**PATH C:\laragon\www\registro_visitas\resources\views/template_parts/sidebar.blade.php ENDPATH**/ ?>