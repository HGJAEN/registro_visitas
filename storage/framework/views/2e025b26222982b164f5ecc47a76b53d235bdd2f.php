<!doctype html>
<html lang="<?php echo e(str_replace('_', '-', app()->getLocale())); ?>">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">

    <title><?php echo e(config('app.name', 'Laravel')); ?></title>
    
    <script>
        let base_path_vue_router = '' ;
        let base_path_asset = "<?php echo e(asset('')); ?>";
    </script>
    <?php if(!empty(Config::get('vueconfig.path'))): ?>
        <script>
            base_path_vue_router = "<?php echo e(Config::get('vueconfig.path')); ?>";
        </script>
    <?php endif; ?>

    <!--BYR-->
    <?php echo app('Tightenco\Ziggy\BladeRouteGenerator')->generate(); ?>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="<?php echo e(asset('css/app.css')); ?>" rel="stylesheet">

    <!-- Styles ICONS Boostrap -->
</head>
<body class="hold-transition sidebar-mini layout-fixed">
    <?php if(auth()->guard()->check()): ?>
        <?php echo $__env->make('template_parts.vue_config', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <?php else: ?>
        <?php echo $__env->make('template_parts.php_config', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <?php endif; ?>

    <!--ROL BY USER-->
    <script>
       window.usauh = "<?php echo !empty(auth()->user()) ? true : false; ?>";
      <?php if(auth()->guard()->check()): ?>
        window.rolbyuser = "<?php echo auth()->user()->getRoleNames()[0]; ?>";
      <?php else: ?>
        window.rolbyuser = '';
      <?php endif; ?>
    </script>
    <!--///END///-->

    <script src="<?php echo e(asset('js/app.js')); ?>" defer></script>
    
    <script>
        // Validamos si existe los mensajes de session para cargar en una vista  y si es asi lo visualizamos
        // Esto es un intento para combinar las session flash de laravel con vuejs al recargar una pagina 
        if(!!document.getElementById("session_message")){

         setTimeout(function(){ document.getElementById("session_message").style.display = "none" }, 5000);
        }
    </script>
</body>
</html>
<?php /**PATH C:\laragon\www\registro_visitas\resources\views\layouts\app.blade.php ENDPATH**/ ?>