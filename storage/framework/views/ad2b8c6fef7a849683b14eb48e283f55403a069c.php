<!-- Navbar -->
<nav class="main-header navbar navbar-expand sticky-top navbar-light">
  <!-- Left navbar links -->
  <ul class="navbar-nav">
    <li class="nav-item">
      <a class="nav-link" data-widget="pushmenu" href="#" role="button"><span class="navbar-toggler-icon"></span></a>
    </li>

  </ul>

  <!-- Right navbar links -->
  <ul class="navbar-nav ml-auto">
    <?php if(auth()->guard()->guest()): ?>
          <?php if(Route::has('login')): ?>
              <li class="nav-item">
                  <a class="nav-link btn btn-primary" href="<?php echo e(route('login')); ?>"><?php echo e(__('Login')); ?></a>
              </li>
          <?php endif; ?>
              <p>Correcto</p>
          <?php if(Route::has('register')): ?>
              <li class="nav-item">
                  <a class="nav-link" href="<?php echo e(route('register')); ?>"><?php echo e(__('Register')); ?></a>
              </li>
          <?php endif; ?>
      <?php else: ?>
          <li class="nav-item dropdown">
              <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre><?php echo e(Auth::user()->name); ?></a>
              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                  <a class="dropdown-item" href="<?php echo e(route('logout')); ?>"
                     onclick="event.preventDefault();
                                   document.getElementById('logout-form').submit();">
                      <?php echo e(__('Cerrar sesión')); ?>

                      <span><i class="bi bi-box-arrow-right"></i></span>
                      
                  </a>

                  <form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" class="d-none">
                      <?php echo csrf_field(); ?>
                  </form>
              </div>
          </li>
      <?php endif; ?>
    <li class="nav-item">
      <a class="nav-link" data-widget="fullscreen" href="#" role="button">
        <i class="bi bi-arrows-fullscreen"></i>
      </a>
    </li>

  </ul>
</nav>
<!-- /.navbar -->
<?php /**PATH C:\laragon\www\registro_visitas\resources\views\template_parts\nav.blade.php ENDPATH**/ ?>