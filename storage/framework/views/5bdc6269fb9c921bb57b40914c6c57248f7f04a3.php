<nav class="navbar navbar-expand-md navbar-light  bg-primary shadow-sm">
    <div class="container">
        <a class="navbar-brand text-white" href="<?php echo e(url('/')); ?>">
            <?php echo e(config('app.name', 'Laravel')); ?>

        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="<?php echo e(__('Toggle navigation')); ?>">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Left Side Of Navbar -->
            <ul class="navbar-nav mr-auto">

            </ul>


        </div>
    </div>
</nav>
<br><br><br>
<div id="app">
  <main >
    <?php echo $__env->yieldContent('content'); ?>
  </main>
</div>
<?php /**PATH C:\laragon\www\registro_visitas\resources\views/template_parts/php_config.blade.php ENDPATH**/ ?>