<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Registro de visitas</title>

    <script>
        let base_path_vue_router = '';
        let base_path_asset = "{{ asset('') }}";
    </script>
    @if (!empty(Config::get('vueconfig.path')))
        <script>
            base_path_vue_router = "{{ Config::get('vueconfig.path') }}";
        </script>
    @endif

    <!--BYR-->
    @routes
    
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    {{-- <link href="{!! asset('css/landing.css') !!}" rel="stylesheet" type="text/css"> --}}
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,300i,400,500,700&display=swap" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

</head>

<body>
    <nav class="navbar navbar-primary">
        <div class="container">
            <h5 class="navbar-brand">Hospital General de Jaén</h5>
            <div class="d-flex">
                @if (Route::has('login'))
                    @auth
                        <span class="navbar-text">
                            <a class="btn btn-secondary text-white" href="{{ url('/home') }}">Inicio</a>
                        </span>
                    @else
                        <span class="navbar-text">
                            <a class="btn btn-primary  text-white" href="{{ route('login') }}">Iniciar Sesión</a>
                        </span>
                    @endauth
                @endif
            </div>
        </div>
    </nav>

    @extends('publico')

    @section('content')
        <!-- Main content -->
        <div class="container-fluid px-md-5">
            <h3 class="h3 fw-bold text-info">Reporte de visitas</h3>
            @if (session('message_success'))
                <div class="container mt-1" id="session_message">
                    <div class="alert alert-success"><i class="bi bi-check-circle"></i> {{ session('message_success') }}
                    </div>
                </div>
            @endif

            <example-component></example-component>
        </div>
        <!-- /.content -->
    @endsection

</body>

</html>
